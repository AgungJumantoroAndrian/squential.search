package BinarySearch;
public class praktikum1 {
    public static void main(String args[]){
        System.out.println("====BINARY SEARCH====+\n");
        int A[]={12,22,24,34,54};
        int cari =12;
        int batasbawah = A.length-1;
        int batasatas = 0;
        boolean ketemu =false;
        int tengah=0;
        
        System.out.print("isi data : ");
        for(int i=0; i<A.length; i++){
            System.out.print(A[i]+", ");
        }
        System.out.println("");
                
        while((batasatas<=batasbawah)&&(ketemu==false)){
            tengah=(batasatas+batasbawah)/2;
            if(A[tengah]==cari){
                ketemu=true;break;
            }else{
                if(A[tengah]<=cari){
                    batasatas=tengah+1;
                }else{
                    batasbawah=tengah-1;                    
                }
            }
        }
        if(ketemu){
            System.out.println("Data "+cari+" Telah ditemukan pada index ke-"+(tengah+", baris ke-"+(tengah+1)));            
        }else{
            System.out.println("Data "+cari+" Tidak ditemukan");
        }
        System.out.println("\n");
        System.out.println("++++TERIMA KASIH+++");        
    }    
}