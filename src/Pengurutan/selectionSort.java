package Pengurutan;
import java.util.Scanner;
public class selectionSort {
    public static void main(String args[]){
        Scanner input= new Scanner(System.in);
        System.out.print("Masukkan angka :");
        int angka =input.nextInt();
        int[]data= new int[angka];
        
        System.out.println("");
        for(int x=0; x<angka; x++){
            System.out.print("input nilai ke-"+(x+1)+" : ");
            data[x]=input.nextInt();
        }
        System.out.println("");
        System.out.print("data :");
        for(int x=0; x<angka; x++)
            System.out.print(data[x]+", ");
        
        System.out.println("\n\nProses Selection Sort");
            System.out.println("___ASCENDING__");
        System.out.println(", ");
        for(int x=0; x<angka; x++){
            System.out.println("Iterasi ke-"+(x+1));
            for(int y=0; y<angka; y++)
                System.out.print(data[y]+" ");
            
            System.out.println("Apakah Data"+ data[x]+" sudah benar urutannya?");
                boolean tukar = false;
                int index = 0;
                int min = data[x];
                String pesan = " Tidak ada pertukaran ";
                for(int y=x+1; y< angka; y++){
                    if(min>data[y]){
                        tukar = true;
                        index = y;
                        min = data[y];
                    }
                }
                if(tukar==true){
                    pesan =" Data "+data[x]+"ditukar dengan Data "+data[index];
                    int temp = data[x];
                    data[x]=data[index];
                    data[index]=temp;
                }
                for(int y=0; y<angka;y++)
                    System.out.print(data[y]+" ");
                
                System.out.println(pesan+"\n");
        }
        System.out.print("Data setelah di sorting : ");
        for(int x=0; x<angka; x++)
            System.out.print(data[x]+" ");
        
        System.out.println("\n\n");
        
         System.out.println("___DISCENDING__");
        System.out.println(", ");
        for(int x=0; x<angka; x++){
            System.out.println("Iterasi ke-"+(x+1));
            for(int y=0; y<angka; y++)
                System.out.print(data[y]+" ");
            
            System.out.println("Apakah Data"+ data[x]+" sudah benar urutannya?");
                boolean tukar = false;
                int index = 0;
                int min = data[x];
                String pesan = " Tidak ada pertukaran ";
                for(int y=x+1; y< angka; y++){
                    if(min<data[y]){
                        tukar = true;
                        index = y;
                        min = data[y];
                    }
                }
                if(tukar==true){
                    pesan =" Data "+data[x]+"ditukar dengan Data "+data[index];
                    int temp = data[x];
                    data[x]=data[index];
                    data[index]=temp;
                }
                for(int y=0; y<angka;y++)
                    System.out.print(data[y]+" ");
                
                System.out.println(pesan+"\n");
        }
        System.out.print("Data setelah di sorting : ");
        for(int x=0; x<angka; x++)
            System.out.print(data[x]+" ");
    }
    
    
}
