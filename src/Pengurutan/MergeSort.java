package Pengurutan;
public class MergeSort {
    public static void initializemergeSort(int n[], int l, int h){
        int low = l; int high = h;
        if (low>=high){
            return;
        }
        int middle = (low+high)/2;
        initializemergeSort(n, low, middle);
        initializemergeSort(n, middle + 1, high);
        int end_low = middle;
        int Start_high= middle + 1;
        while ((l<= end_low) && (Start_high<= high)){
            if (n[low]> n[Start_high]){
                low++;
            } else {
                int Temp = n[Start_high];
                for (int k = Start_high - 1; k>= low; k--){
                    n[k+1]=n[k];
                }
                n[low]=Temp;
                low++;
                end_low++;
                Start_high++;
            }
        }
    }
    public static void main(String args[]){
        System.out.println("Mochamad Aufa");
        System.out.println("\n\n");
        System.out.println("Merge Sorting");
        
        int n[]={321,53,13,31,65,74,765,42,543,65,23,657,87,5,568};
        System.out.println("Data = ");
        for(int i=0; i<n.length;i++){
        System.out.print(n[i]+", ");
        }
        System.out.println();
        
        initializemergeSort(n, 0, n.length-1);
        System.out.println();
        System.out.println("Setelah di Urutan DISCENDING = ");
        for(int i=0; i<n.length; i++){
            System.out.print(n[i]+", ");
        }System.out.println("");
    }
}
