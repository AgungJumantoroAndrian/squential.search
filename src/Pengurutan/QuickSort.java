package Pengurutan;

public class QuickSort {
    static void quickSort(int a[], int lo, int hi){
        int i=lo, j=hi, h;
        int pivot=a[lo];
        do{
            while(a[i]<pivot) i++;
            while(a[j]>pivot) j--;
            if (i<=j){
                h=a[i]; a[i]=a[j]; a[j]=h;
                i++; j--;
            }
        }while(i<=j);
        if (lo<j)quickSort(a, lo, j);
        if (i<hi)quickSort(a, i, hi);
    }         
    static void QuickSort(int a[], int lo, int hi){
        int i=lo, j=hi, h;
        int pivot=a[lo];
        do{
            while(a[i]>pivot) i++;
            while(a[j]<pivot) j--;
            if (i<=j){
                h=a[i]; a[i]=a[j]; a[j]=h;
                i++; j--;
            }
        }while(i<=j);
        if (lo<j)QuickSort(a, lo, j);
        if (i<hi)QuickSort(a, i, hi);
    }         
    public static void main(String[]args){
        int tabInt []={2,1,4,5,6,3,7,9,8,10};
        int i,n=10;
        System.out.print("Data : ");
            for(i=0; i<n; i++){
                System.out.print(tabInt[i]+" ");
            }
            System.out.println("\n");
            System.out.println("__ASCENDING__");
        quickSort(tabInt,0,n-1);       
        for(i=0; i<n; i++){
            System.out.print(tabInt[i]+" ");
        }    
            System.out.println("\n");
            System.out.println("__DISCENDING__");
        QuickSort(tabInt,0,n-1);       
        for(i=0; i<n; i++){
            System.out.print(tabInt[i]+" ");
        }
    }
}
