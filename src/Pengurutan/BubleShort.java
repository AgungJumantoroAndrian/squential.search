package Pengurutan;
import java.util.Arrays;
public class BubleShort {
    public static void main(String args[]){
        int[]bilangan = {3,43,54,1,2};
        System.out.println("Bilangan sebelum Array : "+Arrays.toString(bilangan));
        System.out.println("\nProses Bubble Sort secara Ascending : ");
        for(int i=0; i<bilangan.length; i++){
            System.out.println("Iterasi "+(i+1));
            for(int j=0; j<bilangan.length-1; j++){
                if(bilangan[j]> bilangan[j+1]){
                    int temp = bilangan[j];
                    bilangan[j]=bilangan[j+1];
                    bilangan[j+1]=temp;
                }
                System.out.println(Arrays.toString(bilangan));
            }
            System.out.println("");
        }
        System.out.println("Hasil akhir setelah di sorting : "+Arrays.toString(bilangan));
    }       
}
