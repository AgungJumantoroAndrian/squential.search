package Pengurutan;

import java.util.Arrays;

public class BubleShort2 {
    public static void main (String args[]){
        int []A = {4,6,7,31,34,54,12};
        System.out.println("Before : "+ Arrays.toString(A));
        System.out.println("\nProses Buble Sort Secara Descending : ");
        for(int a=0; a<A.length; a++){
            System.out.println("Iterasi : "+(a+1));
            
            for(int b = 0; b< A.length-1; b++){
                if(A[b]<A[b+1]){
                    int B = A[b];
                    A[b]= A[b+1];
                    A[b+1]=B;
                }
                System.out.println(Arrays.toString(A));
            }
            System.out.println("");
        }
    }
}
