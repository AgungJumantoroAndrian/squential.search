package SquentialSearch;

public class Praktikum2 {
    public static int sequentialSearch(String []elements, String target){
        for(int j= 0; j<elements.length;j++){
            if(elements[j].equals(target)){
                return j;
            }
        }
        return -1;
    }
    public static void main(String args[]){
        System.out.println("Jika Angka ke-0 Maka warna tidak ada");
        String[] arr1 = {"blue","red","purple","green"};
        int index = sequentialSearch(arr1, "red");
        System.out.println("\nAngka ke-"+(index+1));
        index= sequentialSearch(arr1,"pink");
        System.out.println("\nAngka ke-"+(index+1));
    }
}
