package SquentialSearch;

public class praktikum1 {
    public static int SequentialSearch(int[]elements, int target){
        for(int j=0; j<elements.length;j++){
            if(elements[j]==target){
                return j;
            }
        }
        return -1;
    }
    public static void main(String[]args){
        System.out.println("Jika angka ke-0 maka angka tidak ada\n");
        int arr1[]={32,43,1,3,34,21};
        int index = SequentialSearch(arr1,21);
        System.out.println("\nAngka ke-"+(index+1));
        index=SequentialSearch(arr1,0);
        System.out.println("\nAngka ke-"+(index+1));
    }
}
