package Rekursif;
import java.util.Scanner;
public class Latihan05 {
    public static void c(int a){
        int d = 1; 
        for(int i=1; i<=a; i++){
            d = i*d;
        } 
        System.out.println(d);
    }
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        System.out.print("Masukkan angka : ");
        int angka=input.nextInt();
        System.out.print("Hasil : ");
        c(angka);
        

    }

}    

