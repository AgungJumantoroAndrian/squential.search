package Rekursif;
import java.util.Scanner;
public class Latihan05v2 {
    public static int A(int i){
        if(i>1){
            return i*A(--i);
        }else{
            return i;
        }
    }
    public static void main (String args[]){
        Scanner input= new Scanner(System.in);
        System.out.print("Masukkan Angka : ");
        int T= input.nextInt();
        System.out.println("Hasil "+A(T));
    }
}
