package Rekursif;

public class Latihan04v2 {
    public static int hitung(int i){
        if(i>1){
            return i+ hitung(--i);
        }else {
            return i;
        }
    }
    public static void main(String args[]){
        System.out.println("jumlah 1 s.d. 10 = "+hitung(10));
    }
}
