package Rekursif;
import java.util.Scanner;
public class Latihan03 {
    public static void cetakAngka (int angka){
        if(angka >=1){
        System.out.print(angka+" ");
        cetakAngka(--angka);
        }
    }
    public static void main(String args[]){
        Scanner input= new Scanner(System.in);
        System.out.print("Masukkan angka : ");
        int N = input.nextInt();
        cetakAngka(N);
    }
}
